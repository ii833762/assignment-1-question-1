# Introduction to Numerical Modelling
# Assignment 1
# Question 1

import math

print ()
# Question 1 a
print ("Question 1a: Calculate exp(2) using a 5-term Taylor series.")
print ()

# Define a 5-term Taylor series for exp(x)
def taylorexp5(x):
    """Calculate the Taylor series of an exponential to 5-term accuracy"""
    taylorexp5 = 1 + x + (x**2)/2 + (x**3)/6 + (x**4)/24
    return taylorexp5

print ("5-term Taylor approximation:")
print ("exp(2) =", taylorexp5(2))

print ()
# Question 1 b
print ("Question 1b: Compare the Taylor approximation with the exact value.")
print ()

# Define the exact value of an exp(x)
def exp(x):
    """Calculate the exact value of an exponential"""
    exp = math.exp(x)
    return exp

# Find the error in a 5-term Taylor approximation of exp(x)
def taylorexp5error(x):
    """Calculate difference between the 5-term Taylor approximation and \
        exact answer"""
    taylorexp5error = taylorexp5(x) - exp(x)
    return taylorexp5error

print ("5-term Taylor approximation:")
print ("exp(2) =", taylorexp5(2))
print ("Exact value:")
print ("exp(2) = ", exp(2))
print ("Error:")
print (taylorexp5error(2))

print ()
# Question 1 c
print ("Question 1c: Increase the number of terms in the Taylor series and \
compare with the exact value.")
print ()

# Define a 6-term Taylor series for exp(x)
def taylorexp6(x):
    """Calculate the Taylor series of an exponential to 6-term accuracy"""
    taylorexp6 = 1 + x + (x**2)/2 + (x**3)/6 + (x**4)/24 + (x**5)/120
    return taylorexp6

# Find the error in a 6-term Taylor approximation of exp(x)
def taylorexp6error(x):
    """Calculate difference between the 6-term Taylor approximation and \
        exact answer"""
    taylorexp6error = taylorexp6(x) - exp(x)
    return taylorexp6error

print ("6-term Taylor approximation:")
print ("exp(2) =", taylorexp6(2))
print ("Exact value:")
print ("exp(2) = ", exp(2))
print ("Error:")
print (taylorexp6error(2))
print ()

# Define a 7-term Taylor series for exp(x)
def taylorexp7(x):
    """Calculate the Taylor series of an exponential to 7-term accuracy"""
    taylorexp7 = 1 + x + (x**2)/2 + (x**3)/6 + (x**4)/24 + (x**5)/120 + \
        (x**6)/720
    return taylorexp7

# Find the error in a 7-term Taylor approximation of exp(x)
def taylorexp7error(x):
    """Calculate difference between the 7-term Taylor approximation and \
        exact answer"""
    taylorexp7error = taylorexp7(x) - exp(x)
    return taylorexp7error

print ("7-term Taylor approximation:")
print ("exp(2) =", taylorexp7(2))
print ("Exact value:")
print ("exp(2) = ", exp(2))
print ("Error:")
print (taylorexp7error(2))
print ()

# Define a 8-term Taylor series for exp(x)
def taylorexp8(x):
    """Calculate the Taylor series of an exponential to 8-term accuracy"""
    taylorexp8 = 1 + x + (x**2)/2 + (x**3)/6 + (x**4)/24 + (x**5)/120 + \
        (x**6)/720 + (x**7)/5040
    return taylorexp8

# Find the error in a 8-term Taylor approximation of exp(x)
def taylorexp8error(x):
    """Calculate difference between the 8-term Taylor approximation and \
        exact answer"""
    taylorexp8error = taylorexp8(x) - exp(x)
    return taylorexp8error

print ("8-term Taylor approximation:")
print ("exp(2) =", taylorexp8(2))
print ("Exact value:")
print ("exp(2) = ", exp(2))
print ("Error:")
print (taylorexp8error(2))

print ()
# Question 1 d
print ("Question 1d: Calculate the exponential of any value using a Taylor \
series of any number of terms.")
print ()

# Define a N-term Taylor series for exp(x)
def taylorexpN(x,N):
    """Calculate the Taylor series of an exponential to N-term accuracy"""
    taylorexpN = 0
    for i in range(0,N):
        taylorexpN += x**i/math.factorial(i)
    return taylorexpN

# Find the error in a N-term Taylor approximation of exp(x)
def taylorexpNerror(x,N):
    """Calculate difference between the N-term Taylor approximation and \
        exact answer"""
    taylorexpNerror = taylorexpN(x,N) - exp(x)
    return taylorexpNerror

print ("Taylor approximation for exp(2) to an accuracy of 10 terms:")
print ("=", taylorexpN(2,10))
print ("Error =", taylorexpNerror(2,10))
print ("Taylor approximation for exp(5) to an accuracy of 20 terms:")
print ("=", taylorexpN(5,20))
print ("Error =", taylorexpNerror(5,20))
print ("Taylor approximation for exp(4) to an accuracy of 8 terms:")
print ("=", taylorexpN(4,8))
print ("Error =", taylorexpNerror(4,8))
print ("Taylor approximation for exp(10) to an accuracy of 20 terms:")
print ("=", taylorexpN(10,20))
print ("Error =", taylorexpNerror(10,20))
print ("Taylor approximation for exp(8) to an accuracy of 16 terms:")
print ("=", taylorexpN(8,16))
print ("Error =", taylorexpNerror(8,16))
